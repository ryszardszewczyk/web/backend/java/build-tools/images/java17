include common-files/base.mk
SHELL := bash

# keep-sorted start
java17-badge: docker_badge_java17
java17-push: docker_push_java17
java17-test-rmi: docker_rmi_test_java17
java17-test: docker_build_test_java17
java17-topic: docker_topic_java17
java17: docker_build_java17
# keep-sorted end
